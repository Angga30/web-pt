<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
	<!-- meta character set -->
	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine or request Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>PT. MetaVision Inovasi Indonesia</title>

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSS ================================================== -->

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

	<!-- Fontawesome Icon font -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" type='text/css'>
	<!-- Fancybox -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.fancybox.css') ?>" type='text/css'>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" type='text/css'>
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.css') ?>" type='text/css'>
	<!-- Slider -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/slit-slider1.css') ?>" type='text/css'>
	<!-- Animaete -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css') ?>" type='text/css'>
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/main3.css') ?>" type='text/css'>
	<!-- buat login -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>" type='text/css'>

	<!-- Modernizer Script for old Browsers -->
	<script src="<?php echo base_url('assets/js/modernizr-2.6.2.min.js'); ?>" text="text/javascript"></script>

</head>

<body id="body">

	<!--
        Fixed Navigation
        ==================================== -->
	<header id="navigation" class="navbar-inverse navbar-fixed-top animated-header">
		<div class="container">
			<div class="navbar-header">

				<!-- logo -->
				<a style="color: white"><img src="<?php base_url(); ?>assets/img/logopt.png" width="60px" height="60px">PT.METAVISION INOVASI INDONESIA</a>
				<!-- /logo -->
			</div>

			<!-- main nav -->
			<div class="geser">
				<nav class="collapse navbar-collapse navbar-right" role="navigation">
					<ul id="nav" class="nav navbar-nav">
						<li><a href="#body">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#service">Service</a></li>
						<li><a href="#testimonials">portofolio</a></li>
						<li><a href="#social">Follow Us</a></li>
						<li><a href="#contact">Contact</a></li>
					</ul>
				</nav>
				<a href="<?= base_url('login') ?>" style="margin-left: 30px; color: white;">Login</a>
			</div>

			<!-- /main nav -->

		</div>
	</header>
	<!--
        End Fixed Navigation
        ==================================== -->

	<main class="site-content" role="main">

		<!--
        Home Slider
        ==================================== -->

		<section id="home-slider">
			<div id="slider" class="sl-slider-wrapper">

				<div class="sl-slider">

					<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">

						<div class="bg-img bg-img-1"></div>

						<div class="slide-caption">
							<div class="caption-content">
								<h2 class="animated fadeInDown">PT. MetaVision Inovasi Indonesia</h2>
							</div>
						</div>

					</div>

				</div>

			</div><!-- /slider-wrapper -->
		</section>

		<!--
        End Home SliderEnd
        ==================================== -->

		<!-- about section -->
		<section id="about">
			<div class="container">
				<div class="row">
					<div class="wow animated">
						<div class="recent-works text-center">
							<div class="sec-title white">
								<h2>About Us</h2>
							</div>
							<div id="works">
								<div class=" work-item">
									<p>Visi : <br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
										eiusmod tempor
										incididunt ut labore et dolore magna aliqua. <br> <br> There are many variations
										of passages of Lorem Ipsum available, but the majority have suffered alteration
										in some form, by injected humour, or randomised words which don't look even
										slightly believable.</p>
								</div>
								<div class="work-item">
									<p>Misi : <br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
										eiusmod tempor
										incididunt ut labore et dolore magna aliqua. <br><br> There are many variations
										of passages of Lorem Ipsum available, but the majority have suffered alteration
										in some form, by injected humour, or randomised words which don't look even
										slightly believable.</p>
								</div>
								<div class="center">
									<img src="<?= base_url('assets/img/struktur.png') ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end about section -->


		<!-- Service section -->
		<section id="service">
			<div class="container">
				<div class="row">

					<div class="sec-title text-center">
						<h2 class="wow animated bounceInLeft">Bidang Bisnis</h2>
						<!-- <p class="wow animated bounceInRight">Diantaranya:</p> -->
					</div>

					<div class="col-md-4 col-sm-6 col-xs-12 text-center wow animated zoomIn">
						<div class="service-item">
							<div class="service-icon">
								<i class="fa fa-laptop fa-3x"></i>
							</div>
							<h3>Solusi teknologi on demand</h3>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.3s">
						<div class="service-item">
							<div class="service-icon">
								<i class="fa fa-clipboard fa-3x"></i>
							</div>
							<h3>Pengembangan produk berbasis riset</h3>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 col-xs-12 text-center wow animated zoomIn" data-wow-delay="0.6s">
						<div class="service-item">
							<div class="service-icon">
								<i class="fa fa-file-code-o fa-3x"></i>
							</div>
							<h3>Pengembangan software dan hardware</h3>
						</div>
					</div>

				</div>
			</div>
		</section>
		<!-- end Service section -->

		<!-- Testimonial section -->
		<section id="testimonials" class="parallax">
			<div class="overlay">
				<div class="container">
					<div class="row">

						<div class="sec-title text-center white wow animated fadeInDown">
							<h2>portofolio</h2>
						</div>

						<div id="testimonial" class="wow animated fadeInUp">
							<div class="testimonial-item text-center">
								<div class="clearfix">
									<span>Telah digunakan pada sejumlah kegiatan resmi Angkatan Darat</span>
									<p>
									<div class="fa fa-circle"> Seldikjab TA 2018 (201 orang)</div><br>
									<div class="fa fa-circle"> Garjas Awal Pasis Dikreg 56 (280 orang)</div><br>
									<div class="fa fa-circle"> Seldikjab Kress Program TA 2018 (274 orang)</div><br>
									<div class="fa fa-circle"> Garjas Akhir Pasis Dikreg 56 (280 orang)</div><br>
									<div class="fa fa-circle"> Garjas Akhir Siswa Sarcab (500 orang)</div><br>
									<div class="fa fa-circle"> Garjas Seldik Seskoad Dikreg 57 (600 orang</div>
									<p>
								</div>
							</div>
							<div class="testimonial-item text-center">
								<div class="clearfix">
									<span>Lebih dari 2000 rekap nilai yang turut menentukan karir telah dicetak
									</span>
								</div>
							</div>
							<div class="testimonial-item text-center">
								<div class="clearfix">
									<span>Telah disosialisasikan dalam berbagai giat di lingkungan Angkatan Darat yang secara kumulatif melibatkan lebih dari 1300 prajurit</span>
									<p>
									<div class="fa fa-circle"> Sosialisasi kepada Satpur/Satbanpur Kodam III/Siliwangi (6 bat @ 30 testee; total 360 orang)</div><br>
									<div class="fa fa-circle"> Sosialisasi kepada casis Seldikjab (267 orang)</div><br>
									<div class="fa fa-circle"> Sosialisasi kepada pasis Seskoad Dikreg 56 (281 orang)</div><br>
									<div class="fa fa-circle"> Sosialisasi kepada Dansat Kopassus (132 orang)</div><br>
									<div class="fa fa-circle"> Sosialisasi kepada satuan jasmani se-Kodam IV/Diponegoro (300 orang)</div>
									</p>
								</div>
							</div>
							<div class="testimonial-item text-center">
								<div class="clearfix">
									<span>Pengadaan tes kesamaptaan jasmani untuk mabes polri</span>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- end Testimonial section -->

		<!-- Social section -->
		<section id="social" class="parallax">
			<div class="overlay">
				<div class="container">
					<div class="row">
						<div class="sec-title text-center white wow animated fadeInDown">
							<h2>FOLLOW US</h2>
						</div>
						<ul class="social-button">
							<li class="wow animated zoomIn"><a href="https://www.youtube.com/c/MetaVisionStudio"><i class="fa fa-youtube-play fa-2x"></i></a></li>
							&ensp;&ensp;&ensp;&ensp;
							<li class="wow animated zoomIn" data-wow-delay="0.3s"><a href="https://www.facebook.com/MetaVisionIndonesia/"><i class="fa fa-facebook fa-2x"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!-- end Social section -->

		<!-- Contact section -->
		<section id="contact">
			<div class="container">
				<div class="row">

					<div class="sec-title text-center wow animated fadeInDown">
						<h2>Contact</h2>
						<p>Leave Message</p>
					</div>

					<div class="col-md-7 contact-form wow animated fadeInLeft">
						<form action="home/tambah" method="post">
							<input type="hidden" name="tanggal" value="<?php echo date("Y-m-d"); ?>">
							<div class="input-field">
								<input type="text" name="name" class="form-control" placeholder="Your Name...">
							</div>
							<div class="input-field">
								<input type="email" name="email" class="form-control" placeholder="Your Email...">
							</div>
							<div class="input-field">
								<input type="text" name="subject" class="form-control" placeholder="Subject...">
							</div>
							<div class="input-field">
								<textarea name="message" class="form-control" placeholder="Messages..."></textarea>
							</div>
							<button type="submit" id="submit" class="btn btn-blue btn-effect">Send</button>
						</form>
					</div>

					<div class="col-md-5 wow animated fadeInRight">
						<address class="contact-details">
							<h3>Contact Us</h3>
							<p><i class="fa fa-pencil"></i><a href="https://goo.gl/maps/yot6qvHhftCybafK6" style="color: black;">Puri Cikoneng
									Indah, Jalan Puri 1 No 8,
									Bojongsoang,<span>Kec. Bojongsoang, Bandung,</span>
									<span>Jawa Barat 40288</span></a>
							</p>
							<p><i class="fa fa-phone"></i><a href="https://wa.me/+6289526412003">0895 2641 2003</a>
							</p>
							<p><i class="fa fa-envelope"></i><a href="mailto:metanovasi@gmail.com" style="color: black;">metanovasi@gmail.com</a></p><br>
						</address>
					</div>

				</div>
			</div>
	</main>

	<footer id="footer">
		<div class="container">
			<div class="row text-center">
				<div class="footer-content">
					<!-- <div class="wow animated fadeInDown">
						<p>newsletter signup</p>
						<p>Get Cool Stuff! We hate spam!</p>
					</div> -->

					<p>PT. MetaVision Inovasi Indonesia &copy; Copyright 2021.</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- Essential jQuery Plugins
		================================================== -->
	<!-- Main jQuery -->
	<script src="<?= base_url('assets/js/jquery-1.11.1.min.js'); ?>" text="text/javascript"></script>
	<!-- Twitter Bootstrap -->
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>" text="text/javascript"></script>
	<!-- Single Page Nav -->
	<script src="<?= base_url('assets/js/jquery.singlePageNav.min.js'); ?>" text="text/javascript"></script>
	<!-- jquery.fancybox.pack -->
	<script src="<?= base_url('assets/js/jquery.fancybox.pack.js'); ?>" text="text/javascript"></script>
	<!-- Owl Carousel -->
	<script src="<?= base_url('assets/js/owl.carousel.min.js'); ?>" text="text/javascript"></script>
	<!-- jquery easing -->
	<script src="<?= base_url('assets/js/jquery.easing.min.js'); ?>" text="text/javascript"></script>
	<!-- Fullscreen slider -->
	<script src="<?= base_url('assets/js/jquery.slitslider.js'); ?>" text="text/javascript"></script>
	<script src="<?= base_url('assets/js/jquery.ba-cond.min.js'); ?>" text="text/javascript"></script>
	<!-- onscroll animation -->
	<script src="<?= base_url('assets/js/wow.min.js'); ?>" text="text/javascript"></script>
	<!-- Custom Functions -->
	<script src="<?= base_url('assets/js/main.js'); ?>" text="text/javascript"></script>
</body>

</html>