<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Admin</title>
    <link href="<?= base_url() ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>vendor/sbadmin/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <script src="<?= base_url() ?>assets/js/all.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" style="color: white;"><img src="<?php base_url(); ?>assets/img/logopt.png" width="80px" height="80px"></a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
        <ul class="navbar-nav ms-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="<?= site_url('login/logout') ?>">Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav"><br><br>
                        <a class="nav-link" href="<?= site_url('dashboard') ?>">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard
                        </a>
                        <!-- <a class="nav-link" href="<?= site_url('register') ?>">
                            <div class="sb-nav-link-icon"><i class="fas fa-registered"></i></div>
                            Register
                        </a> -->
                        <a class="nav-link collapsed active" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePortofolio" aria-expanded="false" aria-controls="collapsePortofolio">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Portofolio
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapsePortofolio" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link active" href="<?= site_url('portofolio') ?>">Data Tables</a>
                                <a class="nav-link" href="<?= site_url('t_portofolio') ?>">Tambah Data</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid px-4">
                    <h1 class="mt-4">Portofolio</h1>
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table me-1"></i>
                            Konten
                        </div>
                        <div class="card-body">
                            <table id="datatablesSimple" style="font-size: 15px;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Judul</th>
                                        <th>Image</th>
                                        <th>Image2</th>
                                        <th>Image3</th>
                                        <th>Caption</th>
                                        <th>Caption2</th>
                                        <th>Caption3</th>
                                        <th>Caption4</th>
                                        <th>Caption5</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1;
                                    foreach ($Portofolio as $adn) { ?>
                                        <tr align="center">
                                            <td><?= $no++; ?></td>
                                            <td><?= $adn['judul']; ?></td>
                                            <td><img src="<?= base_url('assets/img/upload/' . $adn['image']); ?>" style="width: 50px; height: 50px;"></td>
                                            <td><img src="<?= base_url('assets/img/upload/' . $adn['image2']); ?>" style="width: 50px; height: 50px;"></td>
                                            <td><img src="<?= base_url('assets/img/upload/' . $adn['image3']); ?>" style="width: 50px; height: 50px;"></td>
                                            <td><?= $adn['caption'] ?></td>
                                            <td><?= $adn['caption2'] ?></td>
                                            <td><?= $adn['caption3'] ?></td>
                                            <td><?= $adn['caption4'] ?></td>
                                            <td><?= $adn['caption5'] ?></td>
                                            <td align="center">
                                                <button class="btn btn-primary btn-edit" data-toggle="modal" data-target="#modalEdit<?= $adn['id']; ?>"><i class="fa fa-pen"></i></button>
                                                <a href="<?= base_url() ?>portofolio/hapus/<?= $adn['id'] ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </main>
            <!-- modal edit -->
            <?php
            $no = 0;
            foreach ($Portofolio as $adn) : $no++; ?>
                <div class="modal fade" id="modalEdit<?= $adn['id']; ?>" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="dataDosen">Form Edit</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="<?php echo base_url('Portofolio/edit'); ?>">
                                    <input type="hidden" name="id" value="<?= $adn['id'] ?>">
                                    <div class="form-group">
                                        <label class="control-label" for="judul">Judul</label>
                                        <input type="text" name="judul" class="form-control" id="judul" value="<?= $adn['judul'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="image">Image</label>
                                        <input type="file" name="image" class="form-control" id="image" value="<?= $adn['image'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="image2">Image</label>
                                        <input type="file" name="image2" class="form-control" id="image2" value="<?= $adn['image2'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="image3">Image</label>
                                        <input type="file" name="image3" class="form-control" id="image3" value="<?= $adn['image3'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="caption">Caption</label>
                                        <input type="text" name="caption" class="form-control" id="caption" value="<?= $adn['caption'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="caption2">Caption2</label>
                                        <input type="text" name="caption2" class="form-control" id="caption2" value="<?= $adn['caption2'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="caption3">Caption3</label>
                                        <input type="text" name="caption3" class="form-control" id="caption3" value="<?= $adn['caption3'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="caption4">Caption4</label>
                                        <input type="text" name="caption4" class="form-control" id="caption4" value="<?= $adn['caption4'] ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="caption5">Caption5</label>
                                        <input type="text" name="caption5" class="form-control" id="caption5" value="<?= $adn['caption5'] ?>">
                                    </div>

                                    <div class=" modal-footer">
                                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-success"> Simpan </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid px-4">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; PT. MetaVision Inovasi Indonesia 2021</div>
                        <!-- <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>vendor/sbadmin/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>vendor/sbadmin/js/datatables-simple-demo.js"></script>
</body>

</html>