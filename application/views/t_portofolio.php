<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Admin</title>
    <link href="<?= base_url() ?>assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>vendor/sbadmin/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <script src="<?= base_url() ?>assets/js/all.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <!-- Navbar Brand-->
        <a class="navbar-brand ps-3" style="color: white;"><img src="<?php base_url(); ?>assets/img/logopt.png" width="80px" height="80px"></a>
        <!-- Sidebar Toggle-->
        <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
        <ul class="navbar-nav ms-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="<?= site_url('login/logout') ?>">Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav"><br><br>
                        <a class="nav-link" href="<?= site_url('dashboard') ?>">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard
                        </a>
                        <!-- <a class="nav-link" href="<?= site_url('register') ?>">
                            <div class="sb-nav-link-icon"><i class="fas fa-registered"></i></div>
                            Register
                        </a> -->
                        <a class="nav-link collapsed active" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePortofolio" aria-expanded="false" aria-controls="collapsePortofolio">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Portofolio
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapsePortofolio" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="<?= site_url('portofolio') ?>">Data Tables</a>
                                <a class="nav-link active" href="<?= site_url('t_portofolio') ?>">Tambah Data</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="text-left font-weight-bold text-uppercase" style="font-size: 30px;">Form Tambah Portofolio</div>
                    <!-- <form method="post" id="input_form" action="<?= base_url('TambahPort/tambah') ?>"> -->
                    <?= form_open_multipart('T_Portofolio/tambah'); ?>
                    <div class="form-group">
                        <label for="exampleFormControlJudul" class="font-weight-bold">Judul</label>
                        <input type="text" class="form-control" name="judul" id="exampleFormControlJudul" placeholder="Inputkan Judul">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlImage" class="font-weight-bold">Image</label>
                        <input type="file" class="form-control-file" name="image" id="exampleFormControlImage">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlImage" class="font-weight-bold">Image 2</label>
                        <input type="file" class="form-control-file" name="image2" id="exampleFormControlImage">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlImage" class="font-weight-bold">Image 3</label>
                        <input type="file" class="form-control-file" name="image3" id="exampleFormControlImage">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlCaption" class="font-weight-bold">Caption</label>
                        <textarea class="form-control" name="caption" id="exampleFormControlCaption" rows="3" placeholder="Inputkan Caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlCaption" class="font-weight-bold">Caption 2</label>
                        <textarea class="form-control" name="caption2" id="exampleFormControlCaption" rows="3" placeholder="Inputkan Caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlCaption" class="font-weight-bold">Caption 3</label>
                        <textarea class="form-control" name="caption3" id="exampleFormControlCaption" rows="3" placeholder="Inputkan Caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlCaption" class="font-weight-bold">Caption 4</label>
                        <textarea class="form-control" name="caption4" id="exampleFormControlCaption" rows="3" placeholder="Inputkan Caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlCaption" class="font-weight-bold">Caption 5</label>
                        <textarea class="form-control" name="caption5" id="exampleFormControlCaption" rows="3" placeholder="Inputkan Caption"></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <?= form_close(); ?>
                    <!-- </form> -->
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid px-4">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; PT. MetaVision Inovasi Indonesia 2021</div>
                        <!-- <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>vendor/sbadmin/js/scripts.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>vendor/sbadmin/js/datatables-simple-demo.js"></script>
</body>

</html>