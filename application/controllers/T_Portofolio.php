<?php

class T_Portofolio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_port", "", TRUE);
    }

    public function index()
    {
        $this->load->view('t_portofolio');
    }

    public function tambah()
    {
        $image = $_FILES['image'];
        if ($image = '') {
        } else {
            $config['upload_path'] = './assets/img/upload';
            $config['allowed_types'] = 'jpg|png|gif';

            $this->load->library('upload', $config);
            $this->upload->do_upload('image');
            $image = $this->upload->data('file_name');
        }

        $image2 = $_FILES['image2'];
        if ($image2 = '') {
        } else {
            $config['upload_path'] = './assets/img/upload';
            $config['allowed_types'] = 'jpg|png|gif';

            $this->load->library('upload', $config);
            $this->upload->do_upload('image2');
            $image2 = $this->upload->data('file_name');
        }

        $image3 = $_FILES['image3'];
        if ($image3 = '') {
        } else {
            $config['upload_path'] = './assets/img/upload';
            $config['allowed_types'] = 'jpg|png|gif';

            $this->load->library('upload', $config);
            $this->upload->do_upload('image3');
            $image3 = $this->upload->data('file_name');
        }

        $data = array(
            'judul' => $this->input->post('judul'),
            'image' => $image,
            'image2' => $image2,
            'image3' => $image3,
            'caption' => $this->input->post('caption'),
            'caption2' => $this->input->post('caption2'),
            'caption3' => $this->input->post('caption3'),
            'caption4' => $this->input->post('caption4'),
            'caption5' => $this->input->post('caption5')
        );

        $this->m_port->tambah($data, 'portofolio');
        redirect('portofolio');
    }
}
