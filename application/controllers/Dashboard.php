<?php

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_user", "", TRUE);
    }
    public function index()
    {
        if ($this->session->userdata('login')) {
            $data['User'] = $this->m_user->getAlluser();
            $this->load->view("dashboard", $data);
        } else {
            $this->load->view('login');
        }
    }

    public function hapus($id)
    {
        $this->m_user->hapus($id);
        redirect('dashboard');
    }
}
