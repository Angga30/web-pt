<?php

class Portofolio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_port", "", TRUE);
    }

    public function index()
    {
        $data['Portofolio'] = $this->m_port->getAllport();
        $this->load->view("portofolio", $data);
    }

    public function hapus($id)
    {
        $this->m_port->hapus($id);
        redirect('portofolio');
    }

    public function edit()
    {
        $id = $this->input->post('id');
        $this->m_port->edit($id);
        redirect('portofolio');
    }
}
