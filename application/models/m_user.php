<?php

class m_user extends CI_Model
{
    public function getAlluser()
    {
        return $this->db->get('user')->result_array();
    }

    public function hapus($id)
    {
        $this->db->delete('user', ['no' => $id]);
    }
}
