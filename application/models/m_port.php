<?php

class m_port extends CI_Model
{
    public function getAllPort()
    {
        return $this->db->get('portofolio')->result_array();
    }

    public function tambah($data)
    {
        $this->db->insert('portofolio', $data);
    }

    public function edit($id)
    {
        $data = [
            'judul' => $this->input->post('judul'),
            'image' => $this->input->post('image'),
            'image2' => $this->input->post('image2'),
            'image3' => $this->input->post('image3'),
            'caption' => $this->input->post('caption'),
            'caption2' => $this->input->post('caption2'),
            'caption3' => $this->input->post('caption3'),
            'caption4' => $this->input->post('caption4'),
            'caption5' => $this->input->post('caption5')
        ];
        $this->db->where('id', $id);
        $this->db->update('portofolio', $data);
    }

    function hapus($id)
    {
        $this->db->delete('portofolio', ['id' => $id]);
    }
}
